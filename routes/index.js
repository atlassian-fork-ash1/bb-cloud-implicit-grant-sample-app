var express = require('express');
var router = express.Router();
var config = require('../config');

var auth_grant = {
    oauth_link: "https://bitbucket.org/account/user/bitbucket-cloud-dev/api",
    response_type: "code",
    type: {
        auth_grant_type : "Implicit Grant",
        definition: "In the implicit flow, instead of issuing the client\n" +
                    "an authorization code, the client is issued an access token directly\n" +
                    "(as the result of the resource owner authorization).  The grant type\n" +
                    "is implicit as no intermediate credentials (such as an authorization\n" +
                    "code) are issued (and later used to obtain an access token).",
        when_to_use: "Clients using scripting language",
        diff_with_other_grants: [
                        "Don't need user initiation",
                        "Authorization server does not authenticate the client",
                        "access_token is exposed as a query parameter after the # in the URL",
                        "Security issues due to <a href='https://tools.ietf.org/html/draft-ietf-oauth-v2-31#section-10.3'>Access Tokens</a> " +
                        "and <a href='https://tools.ietf.org/html/draft-ietf-oauth-v2-31#section-10.16'>Misuse of Access Token to Impersonate Resource Owner in Implicit Flow</a>"
                    ],
        document_link : "https://developer.atlassian.com/cloud/bitbucket/oauth-2/",
        rfc_link: "https://tools.ietf.org/html/draft-ietf-oauth-v2-31#section-1.3.2"
    },
    client: {
        key: config.consumerKey,
        secret: config.consumerSecret
    }
};

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', auth_grant);
});

router.get(`/healthcheck`, function(req, res) {
  res.send('OK');
});

router.get('/oauth-callback', function(req, res) {
    console.log(`Recognized callback-url of Authorization Server. Rendering html file for fragment parsing`);
    console.log(`Since this is an implicit grant, we don't need to do anything here just redirect user to oauth-callback.hbs and that should parse the fragment`);
    res.render('oauth-callback', {
        request: {
            method: "GET",
            call: "https://api.bitbucket.org/2.0/repositories"
        },
        type: {
            auth_grant_type: auth_grant.type.auth_grant_type
        }
    });
});

module.exports = router;
